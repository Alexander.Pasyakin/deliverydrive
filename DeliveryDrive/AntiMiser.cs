﻿///////////////////////////////////
///////////////////////////////////
///////Блокировка приложения///////
///////////////////////////////////
///////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliveryDrive
{
    class AntiMiser
    {
        private DateTime dataBlockApp = new DateTime(2020, 1, 20);

        public void BlockApp()
        {
            if(DateTime.Now == dataBlockApp)
            {
                // Здесь вызов метода, открывающего окно информации о причине блокировки
                // и закрытие всех остальных окон (или блокировка кнопок управления, делай как удобнее)
            }
        }
    }
}
