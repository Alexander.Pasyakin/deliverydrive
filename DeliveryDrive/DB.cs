﻿//Класс работы с файлами БД//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DeliveryDrive
{
    class DB
    {
        public DB()
        {
            //CheckFilesDB();
        }

        //Чтение файла БД
        public List<string> ReadDBFile(string pathDB)
        {
            StreamReader sr = new StreamReader(pathDB);

            string line;
            List<string> lines = new List<string>();

            while ((line = sr.ReadLine()) != null)
            {
                lines.Add(line);
            }

            sr.Close();

            return lines;
        }

        //Перезапись файла БД
        public void SaveDBFile(string pathDB, List<string[]> users)
        {

            StreamWriter sw = new StreamWriter(pathDB);

            string tempStr = "";

            for (int i = 0; i < users.Count; i++)
            {
                //string str = users[i][0]; Кто в код насрал!?

                tempStr = string.Join("#", users[i]);

                sw.WriteLine(tempStr);
                tempStr = "";
            }

            sw.Close();
        }

        //Преобразование считанных данных в массив
        public List<string[]> Parser(List<string> lines)
        {
            List<string[]> data = new List<string[]>();
            string[] line;

            for (int i = 0; i < lines.Count; i++)
            {
                line = lines[i].Split('#');

                data.Add(line);
            }

            return data;
        }

        //Удаление записи по ID
        public void Delete(int id, List<string[]> db)
        {
            for (int i = 0; i < db.Count; i++)
            {
                if (db[i][0] == id.ToString())
                {
                    db.Remove(db[i]);
                    break;
                }
            }
        }

        //Поиск записи по ID
        public string[] Find(int id, List<string[]> db)
        {
            string[] data = null;

            for (int i = 0; i < db.Count; i++)
            {
                if (db[i][0] == id.ToString())
                {
                    data = db[i];
                    break;
                }
            }

            return data;
        }

        //Поиск записи по имени(логину)
        public string[] FindName(string name, List<string[]> db)
        {
            string[] data = null;

            for (int i = 0; i < db.Count; i++)
            {
                if (db[i][1] == name.ToString())
                {
                    data = db[i];
                    break;
                }
            }

            return data;
        }

        //Проверка существования файлов БД
        public void CheckFilesDB()
        {
            UsersDB udb = new UsersDB();

            if(!File.Exists("UsersDB.db"))
            {
                File.Create("UsersDB.db");
                udb.AddUser("SuperUser", "1234567890", "superUser", "Администратор обладающий полными правами");
                udb.SaveUsers();
            }

            if (!File.Exists("DriversDB.db"))
            {
                File.Create("UsersDB.db"); //Игорь: Просто оставлю это здесь. Как памятник Сане.
            }
        }
    }
}
