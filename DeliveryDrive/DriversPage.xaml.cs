﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DeliveryDrive
{
    /// <summary>
    /// Логика взаимодействия для DriversPage.xaml
    /// </summary>
    ///     
    public class Driver
    {
        public string ID { get; set; }
        public string Login { get; set; }
        public string Time { get; set; }
    }

    public partial class DriversPage : Page
    {
        DriversDB driversDB;
        Queue queue;
        string[] statusesArray = new string[] { "В пути", "На паузе", "В очереди" };

        public DriversPage()
        {
            InitializeComponent();

            driversDB = new DriversDB();
            queue = new Queue();

            for (int i = 0; i < driversDB.drivers.Count; i++)
            {
                queue.AddDriverInQueue(driversDB.drivers[i]);
            }

            //driversDB

            /*Driver[] driversArray = new Driver[12] { new Driver { ID = "driver1", Status = "online" }, new Driver { ID = "driver2", Status = "offline" }, new Driver { ID = "driver3", Status = "sleep" }, new Driver { ID = "driver1", Status = "online" }, new Driver { ID = "driver2", Status = "offline" }, new Driver { ID = "driver3", Status = "sleep" }, new Driver { ID = "driver1", Status = "online" }, new Driver { ID = "driver2", Status = "offline" }, new Driver { ID = "driver3", Status = "sleep" }, new Driver { ID = "driver1", Status = "online" }, new Driver { ID = "driver2", Status = "offline" }, new Driver { ID = "driver3", Status = "sleep" } };*/
            

            Driver[] driversArray = new Driver[queue.queueDrivers.Count];

            for (int i = 0; i < queue.queueDrivers.Count; i++)
            {
                driversArray[i] = new Driver { ID = queue.queueDrivers[i][0], Login = queue.queueDrivers[i][1], Time = queue.queueDrivers[i][2] } ;
            }

            dataGridDriversInQueue.ItemsSource = driversArray;
            cbStatuses.ItemsSource = statusesArray;
            cbStatuses.SelectedItem = statusesArray[0];

        }

        private void BtnExit_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new AuthPage());
        }

        private void BtnAddDriver_Click(object sender, RoutedEventArgs e)
        {
            AddDriverWindow addDriverWindow = new AddDriverWindow();
            //addDriverWindow.Owner = thi;
            addDriverWindow.ShowDialog();

        }

        private void BtnAdminPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new AdminPage());
        }

        private void QueueRow_Click(object sender, RoutedEventArgs e)
        {
            var currentDriver = (Driver)dataGridDriversInQueue.SelectedValue;

            lbDriverID.Content = currentDriver.Login;
            lbDriverTimeValue.Content = currentDriver.Time;
            cbStatuses.SelectedItem = statusesArray[2];
        }

        public void UpdateDataGrid()
        {

            Driver[] driversArray = new Driver[queue.queueDrivers.Count];

            for (int i = 0; i < queue.queueDrivers.Count; i++)
            {
                driversArray[i] = new Driver { ID = queue.queueDrivers[i][0], Login = queue.queueDrivers[i][1], Time = queue.queueDrivers[i][2] };
            }

            dataGridDriversInQueue.ItemsSource = driversArray;
        }
    }
}
