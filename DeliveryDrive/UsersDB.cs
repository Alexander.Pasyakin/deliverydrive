﻿/////////////////////////////////////////////
/////////////////////////////////////////////
///////Работа с записями пользователей///////
/////////////////////////////////////////////
/////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DeliveryDrive
{
    class UsersDB
    {
        DB db;

        private string pathDB = "UsersDB.db"; // Путь к файлу БД
        
        public static List<string[]> users; //Лист пользователей

        public UsersDB()
        {
            db = new DB();
            users = new List<string[]>();
            CheckFilesDB();
            ReadUsers();
        }

        //Чтение файла пользователей
        public void ReadUsers()
        {
            Parser(db.ReadDBFile(pathDB));
        }

        //Преобразование считанных данных в массив пользователей
        public void Parser(List<string> lines)
        {
            users = db.Parser(lines);
        }

        //Перезапись файла пользователей
        public void SaveUsers()
        {
            db.SaveDBFile(pathDB, users);
        }

        //Поиск пользователя по ID
        public string[] FindUser(int id)
        {
            return db.Find(id, users);
        }


        //Добавление пользователя
        public void AddUser(string name, string password, string role, string comment)
        {
            string[] user = new string[5];

            if (users.Count == 0)
            {
                user[0] = 0.ToString();
            }
            else
            {
                user[0] = (Convert.ToInt32(users.Last()[0]) + 1).ToString();
            }
            
            user[1] = name;
            user[2] = password;
            user[3] = role;
            user[4] = comment;

            users.Add(user);

            SaveUsers();
        }

        //Удаление пользователя по ID
        public void DeleteUser(int id)
        {
            db.Delete(id, users);
        }

        //Авторизация пользователя
        public string Authorization(string login, string password)
        {
            string[] user = db.FindName(login, users);

            if (user == null)
            {
                return "error";
            }

            if(password == user[2])
            {
                return "success";
            }
            else
            {
                return "error";
            }
        }
        public void CheckFilesDB()
        { 
            if (!File.Exists(pathDB))
            {
                File.Create(pathDB).Close();
                AddUser("SuperUser", "1234567890", "superUser", "Администратор обладающий полными правами");
            }
        }

    }
}
