﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DeliveryDrive
{
    /// <summary>
    /// Логика взаимодействия для AddDriverWindow.xaml
    /// </summary>
    public partial class AddDriverWindow : Window
    {
        DriversDB driversDB;
        public AddDriverWindow()
        {
            InitializeComponent();

            driversDB = new DriversDB();
        }

        private void BtnAddDriver_Click(object sender, RoutedEventArgs e)
        {
            driversDB.AddUser(tbID.Text, tbPassword.Text);
            DriversPage.UpdateDataGrid();
            this.Close();
        }
    }
}
