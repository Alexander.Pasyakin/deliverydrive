﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DeliveryDrive
{
    /// <summary>
    /// Логика взаимодействия для AuthPage.xaml
    /// </summary>
    public partial class AuthPage : Page
    {
        UsersDB usersDB; 

        public AuthPage()
        {
            InitializeComponent();

            usersDB = new UsersDB();
        }

        private void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            string authMsg = usersDB.Authorization(tbLogin.Text, tbPassword.Text);
            if (authMsg == "error")
            {
                MessageBox.Show("Неверный логин/пароль", "Ошибка авторизации", MessageBoxButton.OK , MessageBoxImage.Error);
            }
            if (authMsg == "success")
            {
                NavigationService.Navigate(new DriversPage());
            } 
        }
    }
}
