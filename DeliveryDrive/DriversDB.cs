﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DeliveryDrive
{
    class DriversDB
    {
        DB db = new DB();

        private string pathDB = "DriversDB.db"; // Путь к файлу БД

        public List<string[]> drivers; // Лист водителей

        public DriversDB()
        {
            db = new DB();
            drivers = new List<string[]>();
            CheckFilesDB();
            ReadDrivers();
        }

        //Чтение файла водителей
        public void ReadDrivers()
        {
            Parser(db.ReadDBFile(pathDB));
        }

        //Преобразование считанных данных в массив водителей
        public void Parser(List<string> lines)
        {
            drivers = db.Parser(lines);
        }

        //Перезапись файла водителей
        public void SaveDrivers()
        {
            db.SaveDBFile(pathDB, drivers);
        }

        //Поиск водителя по ID
        public string[] FindDriver(int id)
        {
            return db.Find(id, drivers);
        }

        //Добавление водителя
        public void AddUser(string name, string password, string comment = "-") //Игорь: А почему юзеры то? Почему не драйверы??? В названии одно в коде другое. 
        {
            string[] driver = new string[5];

            if (drivers.Count == 0)
            {
                driver[0] = 0.ToString();
            }
            else
            {
                driver[0] = (Convert.ToInt32(drivers.Last()[0]) + 1).ToString();
            }

            //driver[0] = (Convert.ToInt32(drivers.Last()[0]) + 1).ToString();
            driver[1] = name;
            driver[2] = password;
            driver[4] = comment;

            drivers.Add(driver);

            db.SaveDBFile(pathDB, drivers);
        }

        //Удаление водителя по ID
        public void DeleteUser(int id)
        {
            db.Delete(id, drivers);
        }

        public void CheckFilesDB()
        {
            if (!File.Exists("DriversDB.db"))
            {
                File.Create("DriversDB.db").Close();
            }
        }
    }
}
