﻿///////////////////////////////
///////////////////////////////
///////Очередь водителей///////
///////////////////////////////
///////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliveryDrive
{
    class Queue
    {
        public List<string[]> queueDrivers = new List<string[]>(); // Очередь водителей
        public List<string[]> driversWay = new List<string[]>(); // Водители в пути
        public List<string[]> driversPause = new List<string[]>(); // Водители на паузе

        // Добавление водителя в конец очереди
        public void AddDriverInQueue(string[] driver)
        {
            string[] driverInQueue = new string[3];

            driverInQueue[0] = driver[0]; // ID водителя
            driverInQueue[1] = driver[1]; // Имя/логин водителя
            driverInQueue[2] = DateTime.Now.ToShortTimeString(); // Время в пути

            queueDrivers.Add(driverInQueue);
        }

        // Удаление из очереди первого водителя
        public void PullFirstDriver()
        {
            driversWay.Add(queueDrivers.First());

            queueDrivers.RemoveAt(0);
        }

        // Поставить водителя на паузу
        public void PauseDriver(string[] driver)
        {
            DB db = new DB();

            driversPause.Add(db.FindName(driver[1], queueDrivers));
            queueDrivers.Remove(driver);
        }

        // Проверка пароля администратора
        public string CheckAdmin(string login, string password) // Нужно передать логин текущего авторизованного администратора
        {
            DB db = new DB();

            string[] user = db.FindName(login, UsersDB.users);

            if (password == user[2])
            {
                return "OK";
            }
            else
            {
                return "Password failed";
            }
        }

        // Проверка пароля водителя
        public string CheckDriver(string login, string password)
        {
            DB db = new DB();
            DriversDB driversDB = new DriversDB();

            string[] user = db.FindName(login, driversDB.drivers);

            if (password == user[2])
            {
                return "OK";
            }
            else
            {
                return "Password failed";
            }
        }
    }
}
